package com.company;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Numbers {
        public static String readFileAsString(String fileName) throws Exception {
            String data = "";
            data = new String(Files.readAllBytes(Paths.get("numbers.txt")));
            return data;
        }
        public static int[] stringToArray(String line) {
            String clearLine = line.replaceAll("\\s+","").replaceAll("\\.","").replaceAll("[^0-9,]+","");
            int[] numbers = Arrays.stream(clearLine.split(",")).mapToInt(Integer::parseInt).toArray();
            return numbers;
        }
        public static Map<Integer, String> textMap () {
            Map<Integer,String> mymap = new HashMap<>();
            mymap.put(10,"ten");
            mymap.put(20,"twenty");
            mymap.put(30,"thirty");
            mymap.put(40,"forty");
            mymap.put(50,"fifty");
            mymap.put(60,"sixty");
            mymap.put(70,"seventy");
            mymap.put(80,"eighty");
            mymap.put(90,"ninety");
            mymap.put(100,"one hundred");
            mymap.put(110,"one hundred ten");
            mymap.put(120,"one hundred twenty");
            mymap.put(130,"one hundred thirty");
            mymap.put(140,"one hundred forty");
            mymap.put(150,"one hundred fifty");
            mymap.put(160,"one hundred sixty");
            mymap.put(170,"one hundred seventy");
            mymap.put(180,"one hundred eighty");
            mymap.put(190,"one hundred ninety");
            mymap.put(200,"two hundred");
            return mymap;
        }
        public static String outputArray (int[] inputArray) {
            String outputString = "";
            for (int i=inputArray.length-1; i >= 0; i--){
                if (inputArray[i] % 10 == 0){
                    outputString += textMap().get(inputArray[i]);
                }
                else {
                    outputString += inputArray[i];
                }
                outputString += ", ";
            }
            String newString = outputString.substring(0,outputString.length()-2);
            newString += ".";
            return newString;

        }
        public static void outputFile (String content) throws IOException {
            String path = "numbers_new.txt";
            Files.write( Paths.get(path), content.getBytes(), StandardOpenOption.CREATE);

        }

        public static void main(String[] args) throws Exception {
            String data = readFileAsString("numbers.txt");
            int[] numbers = stringToArray(data);
            String outputData = outputArray(numbers);
            outputFile(outputData);
            System.out.println(outputData);


        }

    }

